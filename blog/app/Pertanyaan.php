<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\User;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = [
        'id',
        'judul',
        'isi',
        'hashtag',
        'user_id'
    ];

    // relasi ke profile
    public function profile(){
        return $this->belongsTo(Profile::class);
    }
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function jawaban(){
        return $this->hasMany(Jawaban::class, 'pertanyaan_id');
    }
    
    // tags many to many
    public function tags(){
        // mencari pertanyaan_id             
        // nama table dan foreign key dan foreign key dari post
        return $this->belongsToMany(Tag::class,'pertanyaan_tag','pertanyaan_id','tag_id');
    }
}
