<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/master', function () {
    return view('layout.master');
});

//Login
Auth::routes();
Route::get('/forum', 'HomeController@index')->name('forum');

//Profile
//read and edit profile by profile id 
Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
//update data berdasarkan id
Route::put('/profile/{profile_id}', 'ProfileController@update');

//Forum
Route::get('/forum/create', 'ForumController@create');
Route::post('/forum', 'ForumController@store');
Route::get('/forum/show/{id}', 'ForumController@show');
Route::post('/forum/jawaban/{id}', 'ForumController@jawab');
//Hapus Jawaban dan Pernyaan
Route::post('/forum/hapus/{id}', 'ForumController@destroyJawaban');
Route::delete('/forum/hapus/{id}', 'ForumController@destroyPertanyaan');
//Edit Jawaban dan Pernyaan
Route::get('/forum/edit/{id}', 'ForumController@editPertanyaan'); 
Route::get('/forum/edit2/{id}', 'ForumController@editJawaban');
//Update Jawaban dan Pernyaan
Route::post('/forum/update/{id}', 'ForumController@updatePertanyaan'); 
Route::patch('/forum/update/{id}', 'ForumController@updateJawaban');
