<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{
    protected $table = "profile";
    protected $fillable = [
        'id',
        'name',
        'umur', 
        'photo', 
        'alamat',
        'biodata',
        'email',
        'user_id'
    ];

    // relasi ke table user
    public function user()
    {
        return $this->belongsTo(User::class ,'user_id','id');
    }
    public function pertanyaan(){
        return $this->hasMany(Pertanyaan::class);
    }
    public function getPhoto()
    {
        if (!$this->photo) {
            return asset('photo/1649524066.jpg');
        }
        return asset('photo/'.$this->photo);
    }
}
