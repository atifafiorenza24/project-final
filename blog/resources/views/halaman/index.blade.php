@extends('layout.master')
@section('judul')
Forum 
@endsection
@section('content')
<h1 class="h3 mb-2 text-gray-800">Forum</h1>
{{-- <div class="text-right"> --}}
    <a href="/forum/create" type="submit" class="btn btn-primary btn-sm mb-2">Yuk Tanya!</a>
{{-- </div> --}}
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pertanyaan</h6>
    </div>
    <div class="card-body">
        @foreach ($pertanyaan as $tanya)
            <div class="card mb-2">
                <div class="card-body">
                    <div class="media forum-item">
                        <a href="" data-toggle="collapse" data-target=".forum-content"><img src="{{$tanya->user->profile->getPhoto()}}" class="mr-3 rounded-circle" width="50" alt="user"/></a>
                        <div class="media-body">
                            <h6><a href="forum/show/{{$tanya->id}}" class="text-bold">{{$tanya->judul}}</a></h6>
                            <p class="text-muted"><a href="javascript:void(0)">Created</a> at <span class="text-secondary font-weight-bold">{{$tanya->created_at->diffForHumans()}}</span></p>
                        </div>
                        <div class="text-muted small text-center align-self-center">
                            Tags :
                            {{-- @foreach ($tanya->tags as $tag)
                                <button class="btn btn-primary btn-sm">{{$tag->tag_name ? $tag->tag_name:'No tags'}}</button>
                            @endforeach --}}
                            <button class="btn btn-primary btn-sm">Food</button>
                            <button class="btn btn-primary btn-sm">Restaurant</button>
                            <span><i class="far fa-comment ml-2"></i> 0</span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

