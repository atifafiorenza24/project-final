@extends('layout.master')
@section('judul')
Profile
@endsection
@section('content')
    <!-- Page Heading -->
    <div class="row">

        <div class="col-lg-6">

            <!-- Circle Buttons -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Profile Picture</h6>
                </div>
                <div class="card-body">
                    <img class="img-profile rounded-circle mx-auto d-block" width="150" height="150" src="{{asset('photo/'.$profile->photo)}}">
                    <p class="text-center my-3" >JPG or PNG no larger than 5 MB</p>
                    {{-- <a href="index.html" class="btn btn-primary btn-user btn-block">
                    Upload New Image
                </a> --}}
                </div>
            </div>
        </div>

        <div class="col-lg-6">

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Account Details</h6>
                </div>
                <div class="card-body">
                    <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" value="{{$profile->name}}" class="form-control @error('name') is-invalid @enderror" name="name" id="name">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" value="{{$profile->email}}" class="form-control  @error('email') is-invalid @enderror" name="email" id="email">
                        </div>

                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="text" value="{{$profile->umur}}" class="form-control  @error('umur') is-invalid @enderror" name="umur" id="umur">
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" value="{{$profile->alamat}}" class="form-control  @error('alamat') is-invalid @enderror" name="alamat" id="alamat">
                        </div>
                            
                        <div class="form-group">
                            <label for="biodata">Biodata</label>
                            <input type="text" value="{{$profile->biodata}}" class="form-control" id="biodata" name="biodata">
                        </div>

                        <div class="form-group">
                            <label for="photo">Photo</label>
                            <input type="file" value="{{$profile->photo}}" class="form-control" id="photo" name="photo">
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
