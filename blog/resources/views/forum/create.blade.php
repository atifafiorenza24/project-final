@extends('layout.master')
@section('judul')
Tambah Pertanyaan
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/80drtigxlkdu7ge10qja1mb4hi3tqsislxykf7ydm51lpk5h/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
    </script>
@endpush
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">Masukkan Pertanyaan Anda</h5>
    </div>
    <!-- form start -->
    <form action="/forum" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
             <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul">
            </div>

            @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label for="isi">Pertanyaan</label>
                <textarea name="isi" id="isi" class="form-control my-editor"></textarea>
            </div>
            
            @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label for="hashtag">Hashtag</label>
                <input type="text" class="form-control" id="hashtag" name="hashtag" placeholder="contoh : Food,Restaurant,Computer..">
            </div>

            @error('hashtag')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <a href="/forum" type="submit" class="btn btn-light mt-3">Kembali</a>
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </div>
    </form>
</div>
@endsection
