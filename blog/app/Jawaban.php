<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';
    protected $fillable = ['id','isi','pertanyaan_id','user_id'];

    public function pertanyaan(){
        // foreign key, owner
        return $this->belongsTo(Pertanyaan::class);
    }
    public function user(){
        // foreign key, owner
        return $this->belongsTo(User::class);
    }
}
