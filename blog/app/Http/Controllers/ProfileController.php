<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;
use App\User;

//Sweet alert
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    //login view
    public function create(){
        return view('halaman.profile');
    }

    public function edit($id){
        $profile = Profile::where('user_id', $id)->first();
        return view('halaman.profile', compact('profile'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name'=> 'required',
            'email'=> 'required',
            'umur'=> 'required',
            'photo'=> 'required',
            'alamat'=> 'required',
            'biodata'=> 'required',
        ]);

        $images = time().'.'.$request->photo->extension();
        if($request->hasFile('photo')){
            $request->photo->move(public_path('photo'), $images);
            Profile::where('user_id', $id)
              ->update(
                  [
                  'name' => $request['name'],
                  'email' => $request['email'],
                  'umur' => $request['umur'],
                  'photo' => $images,
                  'alamat' => $request['alamat'],
                  'biodata' => $request['biodata'],
                ]);

            User::where('id', $id)
            ->update(
                [
                'name' => $request->name, 
                'email' => $request->email,
            ]);
        }else{
            Alert::eror('gagal', 'Profile gagal di update');
            return redirect('/profile')->with('eror', 'data anda gagal di update');
        }
        Alert::success('Berhasil', 'Data Berhasil di tambahkan');
        return redirect('/forum');
    }
}
