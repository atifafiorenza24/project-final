<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Profile;
use App\User;
use App\Pertanyaan;
use App\Tag;
use App\Jawaban;

use Illuminate\Support\Facades\Auth;

//Sweet alert
use RealRashid\SweetAlert\Facades\Alert;

class ForumController extends Controller
{
    public function create(){
        return view('forum.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul'=> 'required',
            'isi'=> 'required',
            'hashtag'=> 'required',
        ]);
        
        $tags_arr = explode(',',$request["hashtag"]);

        $tags_id = [];
        foreach($tags_arr as $tag_name){
            // mencari tagname
            $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
            $tags_id[] = $tag->id;
        }

        Pertanyaan::create(
            [
                'judul' => $request['judul'], 
                'isi' => $request['isi'],
                'hashtag' => $request['hashtag'],
                'user_id'=>auth()->user()->id,
                'created_at'=> date('Y-m-d h:i:s'),
                'updated_at'=> date('Y-m-d h:i:s')
            ]
        );

        Alert::success('Berhasil', 'Data Berhasil di tambahkan');
        return redirect('/forum');
    }

    public function show($id){
        $pertanyaan = Pertanyaan::find($id);
        return view('forum.show', compact('pertanyaan'));
    }

    public function jawab(Request $request, $id){
        Jawaban::create(
            [
                'isi' => $request['jawaban'],
                'user_id'=>auth()->user()->id,
                'pertanyaan_id'=>$id,
                'created_at'=> date('Y-m-d h:i:s'),
                'updated_at'=> date('Y-m-d h:i:s')
            ]
        );

        return redirect('/forum/show/'.$id)->with('sukses', 'data anda berhasil di tambahkan');
    }

    public function editPertanyaan($id){
        $user = User::where('id', '!=', Auth::user()->id)->get();
        $pertanyaan = Pertanyaan::find($id);
        return view('forum.edit', compact('pertanyaan','user'));
    }

    public function editJawaban($id)
    {
        $user = User::where('id', '!=', Auth::user()->id)->get();
        $jawaban = Jawaban::find($id);
        return view('forum.editJ', compact('jawaban','user'));
    }

    public function updatePertanyaan(Request $request, $id){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'hashtag' => 'required',
        ]);
        Pertanyaan::where('id', $id)
            ->update(['judul' => $request->judul, 'isi' => $request->isi, 'hashtag' => $request->hashtag]);
        Alert::success('Berhasil', 'Pertanyaan Berhasil di Update');
        return redirect('/forum/show/'.$id);
    }
    public function updateJawaban(Request $request, $id){
        $request->validate([
            'isi' => 'required'
        ]);
        Jawaban::where('id', $id)
            ->update(['isi' => $request->isi]);

        Alert::success('Berhasil', 'Jawaban Berhasil di Update');
        return redirect('/forum/show/'.$request->pertanyaan_id);
    }

    public function destroyPertanyaan($id)
    {
        Pertanyaan::where('id', $id)->delete();
        Alert::success('Berhasil', 'Pertanyaan Berhasil di hapus');
        return back()->with('eror', 'data anda berhasil di hapus');
    }

    public function destroyJawaban($id)
    {
        Jawaban::where('id', $id)->delete();
        Alert::success('Berhasil', 'Jawaban Berhasil di hapus');
        return back()->with('eror', 'data anda berhasil di hapus');
    }
}
