@extends('layout.master')
@section('judul')
Edit Jawaban 
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/80drtigxlkdu7ge10qja1mb4hi3tqsislxykf7ydm51lpk5h/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
    </script>
@endpush
@section('content')
<div class="card card-widget">
    <div class="card-header">
        <form action="/forum/update/{{$jawaban->id}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="jawaban">Edit jawaban anda</label>
            <textarea name="isi" id="isi" class="form-control my-editor">{!!$jawaban->isi!!}</textarea>
                
            @error('jawaban')
                <div class="invalid-feedback mt-2">{{ $message }}</div>
            @enderror
        </div>
        <a href="/forum/show/{{$jawaban->pertanyaan->id}}" type="submit" class="btn btn-light mt-3">Kembali</a>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
    </div>
</div>
@endsection
