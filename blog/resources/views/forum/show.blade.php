@extends('layout.master')
@section('judul')
Details Pertanyaan 
@endsection
@section('content')
<div class="card card-widget">
    <div class="card-header">
        <div class="user-block">
            <h4 class="m-0 font-weight-bold text-primary">{{$pertanyaan->judul}}</h4>
            {{-- <img class="img-circle" src="{{$pertanyaan->user->profile->getPhoto()}}" height="40" width="40" alt="User Image"> --}}
            <span class="username"><a href="#">{{$pertanyaan->user->profile->name}}</a></span>
            <span class="description">{{$pertanyaan->created_at->diffForHumans()}} </span>
        </div>
        <?php $if = App\Pertanyaan::where('user_id','=', $pertanyaan->user->id)->first()?>
        @if ($if->user_id==Auth::user()->id)
        <div class="card-tools">
        <div class="float-right btn-group">
            <button type="button" class="btn btn-tool dropdown-toggle text-dark" data-toggle="dropdown">
            <i class="fas fa-wrench"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right" role="menu">
            <a href="/forum/edit/{{$pertanyaan->id}}" class="dropdown-item">Edit</a>
            <form action="/forum/hapus/{{$pertanyaan->id}}" method="POST">
                @csrf
                <input type="submit" value="Delete" class="dropdown-item btn btn-light btn-sm">
            </form>
            </div>
        </div>
        </div>
        @else

        @endif
    </div>

    <div class="card-body">
        <p>{!!$pertanyaan->isi!!}</p>
        <span class="float-right text-muted">0 comments</span>
    </div>

    {{-- <div class="card-footer card-comments">
        <div class="card-comment">
        <!-- User image -->
        <img class="img-circle img-sm" src="{{asset('photo/1649566876.jpg')}}" height="30" width="30" alt="User Image">

        <div class="comment-text">
            <span class="username">
            username
            <span class="text-muted float-right"></span>
            </span><!-- /.username -->
        </div>
        <!-- /.comment-text -->
        </div>
    </div> --}}
    <!-- /.card-footer -->
    <div class="card-footer">
        <form action="/forum/komentar_pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        <div class="img-push">
            <input type="text" name="komentar" class="form-control form-control-sm" placeholder="Tambah Komentar">
        </div>
        </form>
    </div>
    <!-- /.card-footer -->
    </div>

<br><p>{{$pertanyaan->jawaban->count()}} Jawaban</p>
<div class="card card-widget">
    <div class="card-header">
        <form action="/forum/jawaban/{{$pertanyaan->id}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="jawaban">Masukan jawaban anda</label>
            <textarea name="jawaban" id="jawaban" class="form-control my-editor"></textarea>
                
            @error('jawaban')
                <div class="invalid-feedback mt-2">{{ $message }}</div>
            @enderror
        </div>
    <button type="submit" class="btn btn-primary">Simpan Jawaban</button>
    </form>
    </div>
</div>

@foreach ($pertanyaan->jawaban as $jawab)
        <div class="card card-widget my-3">
                <div class="card-header bg-light">
                    <div class="user-block">
                        <img class="img-circle" src="{{$jawab->user->profile->getPhoto()}}" height="40" width="40" alt="User Image">
                        <span class="username"><a href="#">{{$jawab->user->profile->name}}</a></span>
                        <span class="description">{{$jawab->created_at->diffForHumans()}}</span>
                    </div>
                    <?php $if = App\Jawaban::where('user_id','=', $jawab->user->id)->first()?>
                    @if ($if->user_id==Auth::user()->id)
                        <div class="card-tools">
                            <div class="float-right btn-group">
                                <button type="button" class="btn btn-tool dropdown-toggle text-dark" data-toggle="dropdown">
                                <i class="fas fa-wrench"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <a href="/forum/edit2/{{$jawab->id}}" class="dropdown-item">Edit</a>
                                </form>
                                <form action="/forum/hapus/{{$jawab->id}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <input type="submit" value="Delete" class="dropdown-item btn btn-light btn-sm">
                                </form>
                            </div>
                        </div>
                        </div>
                    @else

                    @endif
                </div>
                <div class="card-body">
                    <p>{!!$jawab->isi!!}</p>
                    <span class="float-right text-muted">0 comments</span>
                </div>
                <div class="card-footer">
                    <form action="" method="POST">
                    @csrf
                    <div class="img-push">
                        <input type="text" name="komentar" class="form-control form-control-sm" placeholder="Tambah Komen">
                    </div>
                    </form>
                </div>
            </div>
@endforeach
@endsection